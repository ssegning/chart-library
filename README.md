# Charter

```bash
helm dependency update charter-helm
````

```bash
helm dependency build charter-helm
````

```bash
helm install charter-helm charter-helm -n charter
````

```bash
helm upgrade charter-helm ./charter-helm -n charter
````

```bash
helm delete charter-helm -n charter
````


- https://faun.pub/my-nginx-configuration-for-angular-6f748a4ff683
- https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
- https://stackoverflow.com/questions/54360223/openshift-nginx-permission-problem-nginx-emerg-mkdir-var-cache-nginx-cli
