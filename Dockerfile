FROM node:14 AS BUILD_IMAGE

WORKDIR /app

COPY ./ssegning-charter .

RUN npm ci && npm run build

FROM nginx:alpine

COPY ./docker/security-headers.conf /etc/nginx/security-headers.conf
COPY ./docker/nginx.conf /etc/nginx/nginx.conf
COPY --from=BUILD_IMAGE /app/dist/ssegning-charter /usr/share/nginx/html

RUN chgrp -R root /var/cache/nginx /var/run /var/log/nginx && \
    chmod -R 770 /var/cache/nginx /var/run /var/log/nginx

CMD ["nginx", "-g", "daemon off;"]
